
public interface MyPredicate<T> {
	public boolean filter(T t);
}
