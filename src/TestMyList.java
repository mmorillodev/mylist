public class TestMyList {
	public static void main(String[] args) {
//		var array = MyList.asList("remover", "sadsd", "remover", "dada", "remover", "dasdas", "reaadfas", "asdasdasd");
//		var tes = MyList.asList(1,2,3,4,5,6);
//		var tes2 = MyList.asList(7,8,9,10);
//		
//		array.removeIf(t -> {
//			if(t.charAt(0) == 'a' || t.equals("remover") || t.charAt(0) == 'd') {
//				return true;
//			}
//			return false;
//		});
//		array.push("Salve", "sou novo aqui men");
//		array.forEach(System.out::println);
//		System.out.println("----------------------");
//		tes.merge(tes2);
//		tes.push(11,12,13,14);
//		tes.forEach(System.out::println);
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		
//		var tesPush = MyList.asList(1,2,4,5,6,7);
//		tesPush.add(6,8);
//		tesPush.add(2,3);
//		tesPush.forEach(System.out::println);
		
		var listBrdelaer = MyList.asList("Multi Zero",
				"Intterpoint",
				"Remax Veiculos",
				"Via Centro Ve�culos",
				"Auto Import Ve�culos",
				"Potente Ve�culos",
				"VM Ve�culos",
				"Brava Autom�veis Ltda",
				"Malibu Motors",
				"Classe A Autom�veis",
				"Tot� Autom�veis",
				"Guaiba Car Multimarcas",
				"Auto Sagui",
				"AM Multimarcas",
				"Fiat Ceolin",
				"Sarvel Ve�culos",
				"Rotta 8 Boqueir�o",
				"Rodecar Autom�veis",
				"Bahia Vip Ve�culos",
				"Vit�ria Ve�culos",
				"Multicar Veiculos",
				"Yellow Car Multimarcas",
				"Guerreiro Autom�veis",
				"Auto Fluxo",
				"Monteiro Ve�culos",
				"JF Veiculos",
				"Cariri Veiculos",
				"Itamarati Ve�culos",
				"Chico Auto",
				"Duas Pistas",
				"Fiat JS Marella",
				"Landing Page - Grupo JSMarella",
				"TurCar",
				"Nova Santa Ve�culos",
				"Dodo Veiculos",
				"Auto Diolli",
				"VM Ve�culos - Lifan",
				"VM Ve�culos - Effa Ve�culos",
				"VM Ve�culos - CN Auto",
				"Portte Autom�veis",
				"Valle Multimarcas",
				"Auto Gallery",
				"Romacar Autom�veis",
				"BBS Ve�culos",
				"BK Autom�veis",
				"Serpin Utilit�rios",
				"Romeiro Multimarcas",
				"Carro RS",
				"Fiat Marina",
				"Almak Ve�culos",
				"ABC Utilit�rios",
				"Vardasca Ve�culos",
				"Ultra Import Ve�culos",
				"Paddock Ve�culos",
				"Med Car Multimarcas",
				"Ricar Ve�culos",
				"Prime Auto Shopping",
				"Automix Ve�culos",
				"Goodcar Santos",
				"Tenda Ve�culos",
				"Nova Status Multimarcas",
				"Connection Car",
				"Globo Multimarcas",
				"Compro Seu Ve�culo",
				"Canico Ve�culos",
				"Placar Multimarcas",
				"Auto Car Ve�culos",
				"Emercar Autom�veis",
				"Econorte Ve�culos",
				"Birautos Ve�culos",
				"Covel Ve�culos",
				"Acrovel Multimarcas",
				"Primer Ve�culos",
				"Vit�ria Multimarcas",
				"Novo Car",
				"Evolution Ve�culos",
				"Bar�o",
				"BRDealer");
		
		var listWaleska = MyList.asList("ABC Utilit�rios",
				"Acrovel Multimarcas",
				"Almak Ve�culos",
				"AM Multimarcas",
				"Auto Car Ve�culos",
				"Auto Diolli",
				"Auto Fluxo",
				"Auto Gallery",
				"Auto Import Ve�culos",
				"Auto Sagui",
				"Automix Ve�culos",
				"Bahia Vip Ve�culos",
				"Bar�o",
				"BBS Ve�culos",
				"Birautos Ve�culos",
				"BK Autom�veis",
				"Brava Autom�veis Ltda",
				"Canico Ve�culos",
				"Cariri Veiculos",
				"Carro RS",
				"Chico Auto",
				"Classe A Autom�veis",
				"Compro Seu Ve�culo",
				"Covel Ve�culos",
				"Dodo Veiculos",
				"Duas Pistas",
				"Econorte Ve�culos",
				"Emercar Autom�veis",
				"Evolution Ve�culos",
				"Fiat Ceolin",
				"Fiat JS Marella",
				"Fiat Marina",
				"Globo Multimarcas",
				"Goodcar Santos",
				"Guaiba Car Multimarcas",
				"Guerreiro Autom�veis",
				"Intterpoint",
				"Itamarati Ve�culos",
				"JF Veiculos",
				"Landing Page - Grupo JSMarella",
				"Malibu Motors",
				"Med Car Multimarcas",
				"Michel Arabe Multimarcas",
				"Monteiro Ve�culos",
				"Multi Zero",
				"Multicar Ve�culos",
				"Nova Santa Ve�culos",
				"Nova Status Multimarcas",
				"Novo Car",
				"Paddock Ve�culos",
				"Placar Multimarcas",
				"Podium Multimarcas",
				"Portte Autom�veis",
				"Potente Ve�culos",
				"Prime Auto Shopping",
				"Prime Rio 4X4",
				"Primer Ve�culos",
				"Remax Veiculos",
				"Ricar Ve�culos",
				"Rodecar Autom�veis",
				"Romacar Autom�veis",
				"Romeiro Multimarcas",
				"Rotta 8 Boqueir�o",
				"Sarvel Ve�culos",
				"Serpin Utilit�rios",
				"Tenda Ve�culos",
				"Tot� Autom�veis",
				"TurCar",
				"Ultra Import Ve�culos",
				"Valle Multimarcas",
				"Vardasca Ve�culos",
				"Via Centro Ve�culos",
				"Visual Multimarcas",
				"Vit�ria Multimarcas",
				"Vit�ria Ve�culos",
				"VM Ve�culos",
				"VM Ve�culos - Lifan",
				"Yellow Car Multimarcas");
		
		listWaleska.forEach(str -> System.out.println(str));
		System.out.println("-----------------------------");
		listWaleska.forEach(new MyConsumer<String>() {
			
			@Override
			public void action(String value) {
				System.out.println(value);
				
			}
		});
		listWaleska.forEach(System.out::println);
	}
}
