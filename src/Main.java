import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;
public class Main {
	public static void main(String[] args) {
		//Lista criada por mim
		MyList<String> array = new MyList<String>();
		//Lista do Java
		ArrayList<String> list = new ArrayList<String>();
		
		//Criando nova thread que passa como parametro uma classe anonima que implementa Runnable
		new Thread(new Runnable(){
			@Override
			public void run() {
				System.out.println("---Thread 1 start.---");
				for(int i = 0; i < 10; i++) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println("---Thread 1 done.---");
			}
			
		}).start();
		
		//Criando nova thread que passa como parametro uma classe anonima que implementa Runnable usando lambda
		new Thread(() -> {
			System.out.println("---Thread 2 start.---");
			for(int i = 0; i < 100; i++) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("---Thread 2 done.---");
		}).start();
		
		//Adicionando elementos na lista do Java
		list.add("sdadasdasdsadsadddsadsads");
		list.add("ola");
		list.add("oi");
		list.add("paralelepipedo");
		list.add("ornitorrinco");
		list.add("arrependimento");
		list.add("desenvolvimento");
		
		//Adicionando novo elemento � cole��o criada por mim e depois retirando o ultimo
		array.push("novo elemento");
		array.push("mais um elemento");
		array.push("elemento que nao vai aparecer");
		
		array.pop();
		
		//Printando os elementos utilizando o metodo toString criado por mim
		System.out.println("---My array to string---");
		System.out.println(array.toString());
		
		//Utilizando metodo Foreach criado por mim passando classe anonima que implementa MyConsumer,
		//para que dentro do metodo possa dar um MyConsumer.action()
		System.out.println("---My array .forEach---");
		array.forEach(new MyConsumer<String>() {
			
			@Override
			public void action(String value) {
				System.out.println(value);
			}
		});
		
		//Apenas chamando action a partir de uma classe anonima
		System.out.println("---My consumer .action('teste')---");
		new MyConsumer<String>() {
			@Override
			public void action(String value) {	
				System.out.println(value);
			}
			
		}.action("teste");
		
		//Dando sort na cole��o do Java passando como parametro uma classe anonima que implementa Comparator,
		//sobrescrevendo o metodo compare com o meu criterio de compara��o
		System.out.println("---Java list sem sort---");
		list.forEach(s -> System.out.println(s));
		
		Collections.sort(list, new Comparator<String>() {

			@Override
			public int compare(String str, String str2) {
				if(str.length() > str2.length()) return 1;
				if(str2.length() > str.length()) return -1;
				return 0;
			}
			
		});
		
		//Printando a lista do Java utilizando o .forEach
		System.out.println("---Java list com sort---");
		list.forEach(s -> System.out.println(s));
	}
}
